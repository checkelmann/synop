FROM python:3.7-slim
WORKDIR /src
ADD synop.py /src
ADD requirements.txt /src
RUN pip install -r requirements.txt
CMD kopf run /src/synop.py