<div align="center">
<img src="https://gitlab.com/checkelmann/synop/raw/38dfcc7c477e191995fc5bb0a58a8181bb7bf2f9/robot.png" alt="Logo" width="200" />
</div>

# Synop

## A Kubernetes Operator to make Ops-Life easier

### What is Synop

Synop is a Kubernetes Operator which is monitoring your K8s cluster for new ingresses and will automatically create a synthetic monitor for them in Dynatrace. No more forgotton checks for new services!

### Prerequirements

You nees at least one ActiveGate installed as this operator currently supports only HTTP-Checks which are not available on public synthetic locations.

### Installation

Follow these Steps to install the Operator

1) Create a Dynatrace API Key with access to this scopes:
   - Create and read synthetic monitors, locations, and nodes
   - Read synthetic monitors, locations and nodes
2) Create a secret which contains your Dynatrace tenant id and API Key.
   - `create_secret.sh abc123 mylongapikey1234`
   - `kubectl apply -f secret_synop.yaml`
3) If you do not want to use the latest development version, check out the [Release Page](https://gitlab.com/checkelmann/synop/-/releases) and adjust the image tag within the synop.yaml file.
4) Install the Operator `kubectl apply -f synop.yaml`

### How it works

Just add the following annotation to your Ingress:

- `synop/create: "true"` - Create a synthetic monitor for this ingress
- `synop/endpoint: "/path/to/check"` - Check this path (default "/")

Optional:

- `synop/name: "myapp"` - Spplication Name (default ingress name)
- `synop/environment: "staging"` - Environment (default namespace)
- `synop/synthetic.tags: "mytag,anothertag,nexttag"` - Additional Tags to add comma seperated (default app:name env:environment)
- `synop/synthetic.frequencyMin: "15"` - Frequence to check (default 15)
- `synop/synthetic.protocol: "https"` - By default synop checks if TLS is configured within your ingress and will use https instead of http. You can overwrite the detection with this parameter.
- `synop/synthetic.locations: "LOCATIONID1234,LOCATIONID95674"` - Comma seperated list if synthetic location IDs, by default synop will check for a private location and will use all private he finds.
- `synop/synthetic.manuallyAssignedApps: "APPLICATION-6734JDLJD89734"` - Comma seperated list of appliaction IDs (default: empty)

``` yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: myapplication
  annotations:
    ingress.kubernetes.io/whitelist-x-forwarded-for: "true"
    kubernetes.io/ingress.class: alb
    alb.ingress.kubernetes.io/scheme: internet-facing
    synop/create: "true"
    synop/endpoint: "/service/ping"
spec:
  rules:
    - http:
        paths:
          - path: /*
            backend:
              serviceName: myapplication
              servicePort: 80

```

### ToDo

- [X] Add possibility to create own tags on the syntetic check
- [X] Overwrite environment with an annotation instead of using the namespace
- [X] Use direct location entityId for synthetic
- [X] Change frequency for checks
- [X] Overwrite used protocol

### References

- [Dynatrace](https://www.dynatrace.com) for sponsoring the development tenant <img src="https://dt-cdn.net/images/dynatrace-logo-rgb-cnh-800x142px-8ad53ac283.svg" alt="Logo" width="200" />

- Zalando for making [kopf](https://github.com/zalando-incubator/kopf).
- Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>